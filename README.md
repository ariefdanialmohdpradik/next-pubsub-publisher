#Pub/Sub - Publisher
The publisher is used by the applications to publish messages to GCP Pub/Sub

##Prerequisites
1. Java 12
2. Maven
3. Credential key to Pub/Sub (.json)

##How to Use
1. Create a project in Google Cloud Platform for Pub/Sub
2. Create Topic & Subscription
3. Generate credential key in .json file 

        IAM & admin > Service Accounts > 'create service account' > 'create key'

4. Include Spring Cloud property in *pom.xml* in the Project Archetype
    ```pom
    <properties>
        ...
        <spring-cloud.version>1.1.1.RELEASE</spring-cloud.version>
        ...
    </properties>
   ```
5. Include Bill-of-Material in *pom.xml* in the Project Archetype
    ```
    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-gcp-dependencies</artifactId>
                <version>${spring-cloud.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>
    ```
6. Include GCP dependencies in *pom.xml* in the Project Archetype
    ```pom
    <dependency>
        <groupId>org.springframework.cloud</groupId>
        <artifactId>spring-cloud-gcp-starter-pubsub</artifactId>
        <version>1.1.3.RELEASE</version>
    </dependency>
    <dependency>
        <groupId>org.springframework.integration</groupId>
        <artifactId>spring-integration-core</artifactId>
        <version>5.1.8.RELEASE</version>
    </dependency>
    ```
7. Define GCP project-id and credential location in *application-local.yml* in the Project Archetype
    ```yaml
    spring:
        cloud:
            gcp:
              project-id: <project-id> # example: sample-project-id
              credentials:
                location: <credential's location> # example: file:src/main/resources/sample-service-account.json
    ```