package th.co.ktb.next.archetype;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.gcp.pubsub.core.PubSubTemplate;
import org.springframework.cloud.gcp.pubsub.integration.inbound.PubSubInboundChannelAdapter;
import org.springframework.cloud.gcp.pubsub.integration.outbound.PubSubMessageHandler;
import org.springframework.cloud.gcp.pubsub.support.BasicAcknowledgeablePubsubMessage;
import org.springframework.cloud.gcp.pubsub.support.GcpPubSubHeaders;
import org.springframework.context.annotation.Bean;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.messaging.MessageChannel;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.messaging.MessageHandler;
import org.springframework.integration.support.utils.PatternMatchUtils;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFutureCallback;
import th.co.ktb.next.archetype.model.request.RequestMessage;

/*
* This class serves as main application.
* It provides Telemetry and Swagger capability with @EnableTelemetry and @EnableSwagger
* */

@Log4j2
@SpringBootApplication
public class BoilerplateServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(BoilerplateServiceApplication.class, args);
	}

	/* SUBSCRIBER */

//	@Bean
//	public MessageChannel myInputChannel(){
//		return new DirectChannel();
//	}
//
//	@Bean
//	public PubSubInboundChannelAdapter messageChannelAdapter(@Qualifier("myInputChannel") MessageChannel inputChannel, PubSubTemplate pubSubTemplate){
//
//	    PubSubInboundChannelAdapter adapter =
//                new PubSubInboundChannelAdapter(pubSubTemplate, "mySubscription");
//	    adapter.setOutputChannel(inputChannel);
//
//	    return adapter;
//    }
//
//    @ServiceActivator(inputChannel = "myInputChannel")
//	public void messageReceiver(@Header(GcpPubSubHeaders.ORIGINAL_MESSAGE) BasicAcknowledgeablePubsubMessage messageAck,
//								RequestMessage requestMessage){
//		log.info("Message arrived! Payload : " + requestMessage);
//	}

	/* PUBLISHER */

//	@Bean
//	@ServiceActivator(inputChannel = "myOutputChannel")
//	public MessageHandler messageSender(PubSubTemplate pubSubTemplate){
//
//		PubSubMessageHandler adapter = new PubSubMessageHandler(pubSubTemplate, "myTopic");
//
//		adapter.setPublishCallback(new ListenableFutureCallback<String>() {
//			@Override
//			public void onFailure(Throwable ex) {
//				log.info("There was an error sending the message.");
//			}
//
//			@Override
//			public void onSuccess(String s) {
//				log.info("Message was sent successfully");
//			}
//		});
//
//		return adapter;
////		return new PubSubMessageHandler(pubSubTemplate, "myTopic");
//	}
//
//	@MessagingGateway(defaultRequestChannel = "myOutputChannel")
//	public interface PubsubOutboundGateway{
//		void sendToPublish(String text);
//	}

}
