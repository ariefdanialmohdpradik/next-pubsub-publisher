package th.co.ktb.next.archetype.adaptor.impl;

import lombok.extern.log4j.Log4j2;
import org.springframework.cloud.gcp.pubsub.core.PubSubTemplate;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.util.MimeTypeUtils;
import th.co.ktb.next.archetype.adaptor.Adaptor;
import th.co.ktb.next.archetype.interceptor.PubInterceptor;
import th.co.ktb.next.archetype.model.request.RequestMessage;
import th.co.ktb.next.archetype.model.response.ResponseMessage;
import th.co.ktb.next.archetype.util.MessageStream;

@Log4j2
@Configuration
@EnableBinding(MessageStream.class)
public class AdaptorImpl implements Adaptor {

    private PubSubTemplate pubSubTemplate;
    private MessageStream messageStream;

    public AdaptorImpl(PubSubTemplate pubSubTemplate, MessageStream messageStream) {
        this.pubSubTemplate = pubSubTemplate;
        this.messageStream = messageStream;
    }

    @Override
    public ResponseMessage sendToPublish(RequestMessage requestMessage) {

        ResponseMessage responseMessage = new ResponseMessage();
        responseMessage.setMessage(requestMessage.getMessage());

        log.info("Message sent : {} , Topic sent : {}", responseMessage, requestMessage.getTopic());

        pubSubTemplate.publish("telemetry", requestMessage);

        return responseMessage;
    }

    @Override
    public ResponseMessage pubInterceptor(RequestMessage requestMessage) {

        pubSubTemplate.publish("telemetry", requestMessage);

        ResponseMessage response = new ResponseMessage();
        response.setMessage("Message : " + requestMessage.getMessage() + " Topic : " + requestMessage.getTopic());

        MessageChannel messageChannel = messageStream.pubGcpInterceptor();

        Message<RequestMessage> msg = MessageBuilder.withPayload(requestMessage)
                .setHeader(MessageHeaders.CONTENT_TYPE, MimeTypeUtils.APPLICATION_JSON)
                .build();

        PubInterceptor pubInterceptor = new PubInterceptor();
        pubInterceptor.preSend(msg, messageChannel);


        return response;
    }
}
