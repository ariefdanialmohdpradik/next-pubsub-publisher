package th.co.ktb.next.archetype.controller;

import org.springframework.web.bind.annotation.*;
import th.co.ktb.next.archetype.model.request.RequestMessage;
import th.co.ktb.next.archetype.model.response.ResponseMessage;
import th.co.ktb.next.archetype.service.PubBaseService;
import th.co.ktb.next.archetype.service.PubInterceptorBaseService;

@RestController
public class MessageController {

    private PubBaseService pubBaseService;
    private PubInterceptorBaseService pubInterceptorBaseService;

    public MessageController(PubBaseService pubBaseService, PubInterceptorBaseService pubInterceptorBaseService) {
        this.pubBaseService = pubBaseService;
        this.pubInterceptorBaseService = pubInterceptorBaseService;
    }

    /* PUBLISHER */
    @PostMapping("/postMessage")
    public ResponseMessage publishMessage(@RequestBody RequestMessage message){
        return pubBaseService.execute(message);
    }

    /* INTERCEPTOR FOR PUBLISHER */
//    @PostMapping("/postMessage")
//    public ResponseMessage publishMessage(@RequestBody RequestMessage message){
//        return pubInterceptorBaseService.execute(message);
//    }


}
