package th.co.ktb.next.archetype.interceptor;

import org.springframework.integration.config.GlobalChannelInterceptor;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.stereotype.Component;
import th.co.ktb.next.archetype.util.MessageStream;

@Component
@GlobalChannelInterceptor(patterns = MessageStream.OUTPUT)
public class PubInterceptor implements ChannelInterceptor {

    @Override
    public Message<?> preSend(Message<?> message, MessageChannel channel) {
        System.out.println("PRE SEND INTERCEPTOR : " + message.getPayload());
        return message;
    }
}
